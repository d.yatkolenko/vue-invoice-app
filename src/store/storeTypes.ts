export interface storeActions {
  commit: (
    name: string,
    val?: any,
    options?: Record<string, unknown>
  ) => () => void;
  dispatch: (
    name: string,
    val?: any,
    options?: Record<string, unknown>
  ) => () => void;
  state: { [key: string]: any };
  getters: { [name: string]: any };
  rootGetters: { [key: string]: () => any };
  rootState: { [key: string]: any };
}
export interface invoice_table_state {
  tableData: invoice_item[];
}
export interface invoice_item extends invoice_new_item {
  id: string | number;
  sum: number;
}
export interface invoice_new_item {
  name: string;
  price: number | string;
  quantity: number | string;
}

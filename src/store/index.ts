import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";
import modules from "@/store/modules/index";

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
  key: "store",
  storage: localStorage,
});

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  modules,
});

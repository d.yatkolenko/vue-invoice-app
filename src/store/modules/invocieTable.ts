import {
  storeActions,
  invoice_table_state,
  invoice_item
} from "@/store/storeTypes";

function randomInteger(min: number, max: number) {
  const rand: number = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

const state: invoice_table_state = {
  tableData: []
};

const getters = {
  getTableData(state: invoice_table_state) {
    return state.tableData;
  },
  getTotalSum(state: invoice_table_state) {
    const total = state.tableData.reduce((a, b) => a + b.sum, 0);
    return total;
  }
};
const mutations = {
  SET_TABLE_DATA(state: invoice_table_state, newData: invoice_item[]) {
    state.tableData = newData;
  },
  ADD_TABLE_ITEM(state: invoice_table_state, newItem: invoice_item) {
    state.tableData.push(newItem);
  }
};
const actions = {
  setTableData({ commit }: storeActions, newData: invoice_item[]) {
    commit("SET_TABLE_DATA", newData);
  },
  addTableItem({ commit }: storeActions, newItem: invoice_item) {
    if (!newItem.id) {
      newItem.id = randomInteger(1, 10000);
    }
    newItem.sum = +newItem.price * +newItem.quantity;
    commit("ADD_TABLE_ITEM", newItem);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
